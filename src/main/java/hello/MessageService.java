package hello;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class MessageService {
    //after : comes the default value.
    @Value("${MESSAGE_PT1:Greetings from spring boot!}")
    private String message1;

    @Value("${MESSAGE_PT2:}")
    private String message2;

    @Value("${MESSAGE_PT3:}")
    private String message3;

    public String getMessage() {
        return message1 + "<br>" + message2 + "<br>" + message3;
    }
}
