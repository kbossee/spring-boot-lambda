package hello;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HelloController.class);

    @Autowired
    private MessageService messageService;

    @RequestMapping("/")
    public String index() {
        String message = messageService.getMessage();
        LOGGER.info("HelloController received message '{}'", message);
        return message;
    }

}