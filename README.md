Simple springboot hello world REST service.

Can be run locally: go to http://localhost:8080/
Optional parameters:

   -DMESSAGE_PT1=message1 -DMESSAGE_PT2=message2 -DMESSAGE_PT3=message3 

You can upload the fat jar 'spring-boot-demo-0.1.0.jar' to AWS and manually create a lambda.
Or you can use serverless (see below) which will also set up API gateway to route the REST call to your lambda.


# AWS Lambda

If you deploy via Serverless, then you can test the service by calling the url.

If you want to test via Lambda console, you first have to create a lambda test event:

- start from 'API Gateway AWS Proxy'
- set httpMethod to GET (near bottom)
- set path to "/" (near bottom)

Note: This example works because there is no properties file, most likely you have this so see HTTP + Cron.


# Serverless

install serverless

    brew install node
    npm install serverless -g

Note: *Serverless uses your default AWS credentials.* For other options see: https://serverless.com/framework/docs/providers/aws/cli-reference/config-credentials/

deploy via serverless

    ./deploy.sh


# SSM config parameters

Environment variable MESSAGE_PT3 is set from the AWS Systems manager parameter store.

- In the AWS console go to Systems Manager
- Find 'Explore Parameter Store' in the dashboard
- Create parameter with name: '/dev/lambdaTestMessage', type: string, value: your message
- redeploy via serverless


# standalone cron job

cron job triggered from CloudWatch, will log message to console every 5 minutes.

- checkout branch cronjob
- ./deploy.sh

manually trigger

    serverless invoke -f cronjob-demo
    
tail logs

    serverless logs -t cronjob-demo -f


# HTTP + Cron

If you want both HTTP + cron, checkout branch combined.
It comes with some minor costs that might need to be considered.

 - The inputstream is saved to a byte array in memory.
 - The StreamLambdaHandler uses the SpringBootLambdaContainerHandler which requires more memory. (1) (2)

(1) SpringLambdaContainerHandler does not load 'application.properties'.
(2) Not that much memory is used actually, it might be CPU related.


# Tear Down
You can remove the application via the CloudFormation console or via serverless.

    serverless remove
    
If you didn't specify your own S3 bucket in the yml: 

- serverless will also remove the generated bucket.
- CloudFormation will fail to delete the bucket because it's not empty. (Empty the bucket and retry.)

Note: serverless invoke or logs does not work when you're on the wrong branch, 
however serverless remove will remove everything, including functions from the other branch.
